using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkController : MonoBehaviour
{
    private GameObject player;
    private int hp = 10;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {   
        if (player == null) {
            // transform.Translate(0, 0, 1);
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hit;

            if (Physics.SphereCast(ray, 5.75f, out hit))
            {
                GameObject hitObject = hit.transform.gameObject;
                if (hitObject.GetComponent<PlayerCharacter>())
                {   
                    Debug.Log("DETECTED");
                    player = hitObject;
                }
            }
            return;
        }

        float x = getMovementDelta(transform.position.x, player.transform.position.x);
        float y = getMovementDelta(transform.position.y, player.transform.position.y);
        float z = getMovementDelta(transform.position.z, player.transform.position.z);

        transform.Translate(x, y, z);
        Vector3 targetDirection = player.transform.position - transform.position;

        // The step size is equal to speed times frame time.
        float singleStep = 0.3f * Time.deltaTime;

        // Rotate the forward vector towards the target direction by one step
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);

        // Draw a ray pointing at our target in
        Debug.DrawRay(transform.position, newDirection, Color.red);

        // Calculate a rotation a step closer to the target and applies rotation to this object
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    private float getMovementDelta(float shark, float player) {
        if (shark > player) return  -0.03f;

        return 0.03f;
    }

    public void react() {
        Debug.Log(this.gameObject.transform.position);
        hp--;
        Debug.Log(hp);
        int angle  = Random.Range(-50, 50);
        this.transform.Rotate(0, 0, angle);
        if (hp == 0) {
            StartCoroutine(Die());
        } 
    }

    private IEnumerator Die()
    {
        
        this.transform.Rotate(-90, 0, 0);
        this.transform.Translate(0, 0,-0.75f);
        yield return new WaitForSeconds(1.5f);
        Destroy(this.gameObject);
    }

    void DisplayHp() {
        Rect hpRect = new Rect(10, 10, 32, 32);
        GUIStyle styles = new GUIStyle();
        styles.fontSize = 30;
        styles.fontStyle = FontStyle.Bold;
        styles.normal.textColor = Color.red;

        GUI.Label(hpRect, hp.ToString(), styles);
    }

    void OnGUI() {
        DisplayHp();
    }
}
