﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    [SerializeField] private GameObject enemyPrefab;
    private GameObject _enemy;

    [SerializeField] private GameObject Health;
    private GameObject hp;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_enemy == null)
        {
            _enemy = Instantiate(enemyPrefab) as GameObject;
            _enemy.transform.position = new Vector3(0, 1, 0);
            float angle = Random.Range(0, 360);
            _enemy.transform.Rotate(0, angle, 0);
        }
        if (hp == null)
        { 
            hp = Instantiate(Health) as GameObject;
            hp.transform.position = new Vector3(Random.Range(0, 20), 0.75f, Random.Range(10, 20));
           
        }


    }
}
