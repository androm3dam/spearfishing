﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class light : MonoBehaviour
{
    Light lt;
    public GameObject Player;
    private float Gh;

    void Start()
    {
        Gh = Player.transform.position.x;
        lt = GetComponent<Light>();
        lt.intensity = 0;
    }

    void Update()
    {
        if (Gh<Player.transform.position.x)
        lt.intensity += 0.01f;
        if (Gh > Player.transform.position.x)
            lt.intensity -= 0.01f;

        Gh = Player.transform.position.x;
    }
}
