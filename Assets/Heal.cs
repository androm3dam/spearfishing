﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : MonoBehaviour
{
   
 
    
    

    void OnTriggerEnter(Collider other)
    { 
        PlayerCharacter player = other.GetComponent<PlayerCharacter>();
        if (player != null)
        {
            player.Hurt(- 1);
            Destroy(this.gameObject);

        }
      
    }
}
